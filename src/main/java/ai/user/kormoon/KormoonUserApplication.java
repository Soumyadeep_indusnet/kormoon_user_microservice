package ai.user.kormoon;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

@SpringBootApplication
@EnableJpaRepositories
@PropertySource(value = "messages.properties")
public class KormoonUserApplication {

	public static void main(String[] args) {

		SpringApplication.run(KormoonUserApplication.class, args);
		
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {

		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {

				registry.addMapping("/**");

			}

		};

	}

	@Bean
	public AcceptHeaderLocaleResolver localeResolver(WebMvcProperties mvcProperties) {

		AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver() {

			@Override
			public Locale resolveLocale(HttpServletRequest request) {

				String locale = request.getParameter("lang");
				return locale != null ? org.springframework.util.StringUtils.parseLocaleString(locale)
						: super.resolveLocale(request);

			}

		};

		localeResolver.setDefaultLocale(mvcProperties.getLocale());
		return localeResolver;

	}

	@Bean
	public CookieSerializer cookieSerializer() {

		DefaultCookieSerializer serializer = new DefaultCookieSerializer();

		serializer.setCookieName("Session");
		serializer.setSameSite("Lax");

		return serializer;
	}

}
