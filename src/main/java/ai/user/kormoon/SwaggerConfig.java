package ai.user.kormoon;

//import static springfox.documentation.builders.PathSelectors.regex;
//
//import java.util.Arrays;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//
//import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
//import springfox.documentation.builders.ParameterBuilder;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.schema.ModelRef;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//@Configuration
//@EnableSwagger2
//@Import(BeanValidatorPluginsConfiguration.class)
//public class SwaggerConfig extends WebMvcConfigurationSupport {
//
//	@Bean
//    public Docket productApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()    
//                .apis(RequestHandlerSelectors.basePackage("com.ams"))
//                .paths(regex("/*.*"))
//                .build()
//                .globalOperationParameters(
//                        Arrays.asList(new ParameterBuilder()
//                            .name("Authorization")
//                            .description("Description of header")
//                            .modelRef(new ModelRef("string"))
//                            .parameterType("header")
//                            .required(true)
//                            .build()));
//             
//    }
//	
//	@Override
//    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("swagger-ui.html")
//                .addResourceLocations("classpath:/META-INF/resources/");
//
//        registry.addResourceHandler("/webjars/**")
//                .addResourceLocations("classpath:/META-INF/resources/webjars/");
//    }
//	
//}
