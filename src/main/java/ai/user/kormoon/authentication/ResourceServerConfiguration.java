package ai.user.kormoon.authentication;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource-server-rest-api";
	private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
	private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
	private static final String SECURED_PATTERN = "/**";

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {

		resources.resourceId(RESOURCE_ID);

	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.requestMatchers().antMatchers(SECURED_PATTERN).and().authorizeRequests()

				.antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll().antMatchers("/login").permitAll()
				.antMatchers("/session-logout").permitAll().antMatchers(HttpMethod.POST, SECURED_PATTERN)
				.access(SECURED_WRITE_SCOPE)
				.anyRequest().access(SECURED_READ_SCOPE)
				.and()
				.addFilterBefore(new SecurityContextRestorerFilter(), AbstractPreAuthenticatedProcessingFilter.class);

		http.sessionManagement().sessionFixation().none();

	}

}
