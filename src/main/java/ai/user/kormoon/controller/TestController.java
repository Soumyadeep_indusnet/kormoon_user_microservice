package ai.user.kormoon.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.user.kormoon.dto.response.UserDTO;
import ai.user.kormoon.entity.UserEntity;
import ai.user.kormoon.enums.RoleConstants;

@RestController
//@RequestMapping("test")
public class TestController {
	
	
	@GetMapping(value = "permit-all")
	public String permitAll(Authentication authentication) {
		
		UserEntity user = (UserEntity) authentication.getPrincipal();
		
		return "Hello, " + user.getFirstName();
		
	}

	@GetMapping(value = "permit-super-admin")
//	@PreAuthorize("!hasAnyRole('" + ROLE_AUDIT_PARTY_IN_CHARGE + "','" 
//														   + ROLE_AUDIT_UNIT_IN_CHARGE + "','" + ROLE_AUDITOR
//														   + "','" + ADMINISTRATIVE_OFFICER + "')"
//														   + " && hasAuthority('"+RO_HO_DASHBOARD+ "_" + VIEW +"' )" ) 
	@PreAuthorize("hasRole('"+RoleConstants.ROLE_SUPER_ADMIN+"')")
	public String permitOnlySuperAdmin(Authentication authentication) {
		
		UserEntity user = (UserEntity) authentication.getPrincipal();

		return "Hello, " + user.getFirstName();
		
	}
	
	@GetMapping(value = "getUserInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO returnData(Authentication authentication) {
		UserEntity user = (UserEntity) authentication.getPrincipal();
		UserDTO dto = new UserDTO();
		dto.setUnserName(user.getUsername());
		dto.setAuthorities(user.getStringAuthorities());
		return dto;
	}
	
}
