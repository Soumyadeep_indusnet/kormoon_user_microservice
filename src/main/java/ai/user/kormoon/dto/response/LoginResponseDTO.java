package ai.user.kormoon.dto.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginResponseDTO {

	Boolean loggedIn = false;
	String access_token;

}
