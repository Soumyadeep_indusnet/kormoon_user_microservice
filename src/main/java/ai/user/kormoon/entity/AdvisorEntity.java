package ai.user.kormoon.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Entity
@Table(name = "tbl_advisor")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = false)
public class AdvisorEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	UserEntity user;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "job_title_id")
	JobTitleEntity jobTitle;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "advisor_type_id")
	AdvisorTypeEntity advisorType;
	
	@Column(name = "company_trading_name")
	String companyTradingName;
	
	@Column(name = "years_of_experience")
	Integer yearsOfExperience;
	
	@Column(name = "is_profile_complete")
	Boolean isProfileComplete;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "tbl_advisor_jurisdiction", joinColumns = @JoinColumn(name = "advisor_id"))
    @Column(name = "jurisdiction")
	Set<String> jurisdictionList = new HashSet<>();
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "tbl_advisor_practice_area", joinColumns = @JoinColumn(name = "advisor_id"))
    @Column(name = "practice_area")
	Set<String> practiceAreaList = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "tbl_advisor_work_experience", joinColumns = @JoinColumn(name = "advisor_id"), inverseJoinColumns = @JoinColumn(name = "work_experience_id"))
	Set<WorkExperienceEntity> workExperienceList = new HashSet<>();
	
}
