package ai.user.kormoon.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import ai.user.kormoon.enums.RiskScore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Entity
@Table(name = "tbl_client_response_settings")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = false)
public class ClientResponseSettingsEntity extends CommonBaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	Long id;

	@Column(name = "risk_score")
	@NotNull
	@Enumerated(EnumType.STRING)
	RiskScore riskScore;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "response_type_id")
	ResponseTypeEntity responseType;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "tbl_client_response_settings_country", joinColumns = @JoinColumn(name = "client_response_settings_id"), inverseJoinColumns = @JoinColumn(name = "country_id"))
	Set<CountryEntity> countryList = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "tbl_client_response_settings_advisor", joinColumns = @JoinColumn(name = "client_response_settings_id"), inverseJoinColumns = @JoinColumn(name = "advisor_id"))
	Set<AdvisorEntity> advisorList = new HashSet<>();
	
}
