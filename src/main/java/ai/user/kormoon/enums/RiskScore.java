package ai.user.kormoon.enums;

public enum RiskScore {
	
	LOW,
	MEDIUM,
	HIGH

}
