package ai.user.kormoon.enums;

public class RoleConstants {

	public static final String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
	public static final String ROLE_USER = "ROLE_USER";
	
}
