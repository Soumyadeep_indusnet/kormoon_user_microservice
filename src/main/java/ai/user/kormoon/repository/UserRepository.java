package ai.user.kormoon.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ai.user.kormoon.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

	Optional<UserEntity> findByUserName(String username);

}
