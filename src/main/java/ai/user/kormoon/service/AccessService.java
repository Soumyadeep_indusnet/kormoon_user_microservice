package ai.user.kormoon.service;

import java.net.URISyntaxException;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import ai.user.kormoon.dto.request.LoginRequestDTO;
import ai.user.kormoon.dto.response.LoginResponseDTO;
import ai.user.kormoon.exception.ForbiddenException;

public interface AccessService {

	public LoginResponseDTO loginByUserName(LoginRequestDTO requestDTO, HttpServletRequest request)
			throws URISyntaxException, ForbiddenException, UnknownHostException;

	public void logout(OAuth2Authentication auth, HttpServletRequest request) throws InterruptedException;

}
